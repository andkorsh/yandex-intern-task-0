package ru.korshunov.yandex_intern_task;

class SnippetJson
{
	private String url;
	private String snippet;
	
	public SnippetJson(String url, String snippet)
	{
		this.url = url;
		this.snippet = snippet;
	}
	
	public String getUrl() {return url;}
	public String getSnippet() {return snippet;}
}