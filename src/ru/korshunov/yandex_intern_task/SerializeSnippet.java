/*
 * Класс реализует сериализацию SnippetJson-объекта
 * в Json-строку со сниппетом. 
 */

package ru.korshunov.yandex_intern_task;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import com.fasterxml.jackson.databind.ObjectMapper;

class SerializeSnippet
{
	protected static String serialize(SnippetJson snippetJson)
	{
		try 
		{
			ObjectMapper mapper = new ObjectMapper();
			Writer stringWriter = new StringWriter();
			mapper.writeValue(stringWriter, snippetJson);
			return stringWriter.toString();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
}