/*
 * Класс реализует основную логику программы
 * 
 * (считывание по одной строке и запуск этого потока, 
 * ожидание каждого потока с получением результатов и 
 * записью в файл) 
 */

package ru.korshunov.yandex_intern_task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main
{
	public static void main(String... h)
	{
		ExecutorService service = Executors.newCachedThreadPool();
		List<Future<String>> futures = new ArrayList<>();
		
		while (JsonRead.hasNext())
			futures.add(service.submit(new Task(JsonRead.next())));

		for (Future<String> future : futures)
		{
			try 
			{
				JsonWrite.write(future.get());
			} 
			catch (InterruptedException | ExecutionException e) 
			{
				e.printStackTrace();
			}
		}
		
		JsonWrite.close();
		service.shutdown();
	}
}
