/*
 * Класс абстрагирует процесс построчного
 * считывания входного файла
 */

package ru.korshunov.yandex_intern_task;

import java.io.FileNotFoundException;
import java.util.Scanner;

class JsonRead
{
	private static Scanner scanner;
	
	static
	{
		try 
		{
			scanner = new Scanner(Options.SOURCE_FILE);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	
	protected static boolean hasNext()
	{
		if (scanner.hasNext())
			return true;
		else
		{
			scanner.close();
			return false;
		}
	}
	
	protected static String next()
	{
		return scanner.nextLine();
	}
}