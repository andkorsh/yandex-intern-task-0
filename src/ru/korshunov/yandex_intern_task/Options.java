package ru.korshunov.yandex_intern_task;

import java.io.File;

class Options
{
	/*
	 * Опции создания сниппета
	 */
	
	protected static final String TO_BE_CONTINUED_STRING = "...";

	protected static final int MAX_LENGTH_OF_SNIPPET = 300 
			- TO_BE_CONTINUED_STRING.length();
		// Чтобы при дописывании "..." длина не привысила 300

	protected static final String TITLE_SNIPPET_SEPARATOR = ". ";

	protected static final char SNIPPET_TRIM_SEPARATOR = ' '; 
		// Пробел - резать сниппет по словам. Точка - по "предложениям".

	protected static final char[] badLastChars = new char[]{',', ';', '(', '"'};
		// Плохие символы для окончания сниппета при обрезке по словам

	protected static boolean TO_BE_CONT_STR_AFTER_POINT = false;
		// Ставить ли TO_BE_CONTINUED_STRING при окончании сниппета на точку

	/*
	 * Пути исходного и результирующего файлов 
	 */
	
	protected static final File SOURCE_FILE = 
			new File("data/intern-task.json.filtered");
		// исходный файл
	
	protected static final File RESULT_FILE = 
			new File("data/snippet.json");
		// результирующий файл
}
