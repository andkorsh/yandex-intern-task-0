/*
 * Класс реализует логику потоковой работы над одной
 * строкой входных данных.
 * 
 * (На вход подаётся json-строка с содержанием статьи, 
 * она десериализуется в ContentJson-объект, по нему 
 * с помощью класса ContentToSnippet создаётся 
 * SnippetJson-объект, сериализуется и получившаяся 
 * строка возвращается пулу потоков)
 */

package ru.korshunov.yandex_intern_task;

import java.util.concurrent.Callable;

class Task implements Callable<String>
{
	private ContentJson contentJson;
	private String jsonString;

	public Task(String jsonString)
	{
		this.jsonString = jsonString;
	}

	@Override
	public String call()
	{
		contentJson = DeserializeContent.deserialize(jsonString);
		String snippet = ContentToSnippet.makeSnippet(contentJson.getTitle(), 
				contentJson.getContent());
		SnippetJson snippetJson = new SnippetJson(contentJson.getUrl(), snippet);
		return SerializeSnippet.serialize(snippetJson);
	}
}
