/*
 * Класс реализует десериализацию Json-строки с содержанием 
 * в ContentJson-объект.
 */

package ru.korshunov.yandex_intern_task;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

class DeserializeContent
{
	public static ContentJson deserialize(String jsonString)
	{
		try 
		{
			ObjectMapper mapper = new ObjectMapper();
			ContentJson contentJson = mapper.readValue(jsonString, ContentJson.class);
			return contentJson;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
}