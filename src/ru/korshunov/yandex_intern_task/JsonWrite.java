/*
 * Класс абстрагирует процесс построчной
 * записи в файл
 */

package ru.korshunov.yandex_intern_task;

import java.io.FileWriter;
import java.io.IOException;

class JsonWrite
{
	private static FileWriter output;
	
	static
	{
		try 
		{
			output = new FileWriter(Options.RESULT_FILE);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	protected static void write(String str)
	{
		try 
		{
			output.write(str+"\n");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	protected static void close()
	{
		try 
		{
			output.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}