/*
 * Класс полностью абстрагирует процесс
 * создания аннотации из заголовка и содержания.
 */

package ru.korshunov.yandex_intern_task;

class ContentToSnippet
{
	private static String removeTags(String str) 
	{
		str = str.replaceAll("\\s{1,}", " ");
		str = str.replaceAll("<.*?>", "");
		return str;
	}

	private static boolean isBadChar(char c)
	{
		for (char temp : Options.badLastChars)
			if (temp == c)
				return true;
		return false;
	}
	
	protected static String makeSnippet(String title, String content)
	{
		content = removeTags(content);
		title = removeTags(title);
		
		String snippet = content;

		if (!(snippet.startsWith(title)))
			snippet = title + Options.TITLE_SNIPPET_SEPARATOR + snippet;

		if (snippet.length() > Options.MAX_LENGTH_OF_SNIPPET)
		{
			snippet = snippet.substring(0, Options.MAX_LENGTH_OF_SNIPPET);

			int trimSeparatorPos = 
				snippet.lastIndexOf(Options.SNIPPET_TRIM_SEPARATOR);
			if (trimSeparatorPos > 0)
				snippet = snippet.substring(0, trimSeparatorPos);

			char lastChar = snippet.charAt(snippet.length() - 1);

			if (isBadChar(lastChar))
				snippet = snippet.substring(0, snippet.length()-1);

			if (lastChar != '.' || Options.TO_BE_CONT_STR_AFTER_POINT)
				snippet = snippet + Options.TO_BE_CONTINUED_STRING;
		}
		
		return snippet;
	}
}